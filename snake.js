"use strict";

(function () {
    /* initialize variables */
    var rows = 15;
    var cols = 15;
    var foodSize = 2;
    var timeIncrement = 300; //ms

    // class Snake
    function Snake(size) {
        this.size = parseInt(size);
        this.positions = genInitPositions(size);
        // once a point in the body of the snake reaches a spin, it canges direction.
        this.spins = [];
        
        this.checkSpin = function(pos){
            this.spins.forEach(function(spin) {
                if (pos.posX == spin.posX && pos.posY == spin.posY) {
                    spin.life--;
                    pos.direction = spin.direction;
                }
            })
            this.spins = this.spins.filter(function(spin) { return spin.life != 0});
        };

        this.movePixel = function() {
            this.positions.forEach(function (pos) {                 
                // look at the new position and check if it steps on a spin
                this.checkSpin(pos);
                if (pos.direction == 'right') {
                    pos.posX = pos.posX + 1;
                }
                if (pos.direction == 'left') {
                    pos.posX = pos.posX - 1;
                }
                if (pos.direction == 'up') {
                    pos.posY = pos.posY - 1;
                }
                if (pos.direction == 'down') {
                    pos.posY = pos.posY + 1;
                }
            }.bind(this));
            checkGameOver(this.positions);
        };

        function genInitPositions(size) {
            var positions = [];
            for (var i = size-1; i >= 0; i--) {
                positions.push({posX: i, posY: 0, direction: 'right'});
            }
            return positions;
        }

        this.getSnakeDirection = function() {
            return this.positions[0].direction;
        };

        this.createSpin = function(new_direc) {
            var spin = {posX: this.positions[0].posX, posY: this.positions[0].posY,
                direction: new_direc, life: this.size}
            this.spins.push(spin);
        }

        this.getFood = function() {
            for (var i = 0; i < foodSize; i ++) {
                var lastSquare = this.positions[this.size - 1];
                switch (lastSquare.direction) {
                    case 'right':
                        this.positions.push({
                            posX: lastSquare.posX - 1, posY: lastSquare.posY,
                            direction: 'right'
                        });
                        break;
                    case 'left':
                        this.positions.push({
                            posX: lastSquare.posX + 1, posY: lastSquare.posY,
                            direction: 'left'
                        });
                        break;
                    case 'up':
                        this.positions.push({
                            posX: lastSquare.posX, posY: lastSquare.posY + 1,
                            direction: 'up'
                        });
                        break;
                    case 'down':
                        this.positions.push({
                            posX: lastSquare.posX, posY: lastSquare.posY - 1,
                            direction: 'down'
                        });
                        break;
                }
                this.size++;
                this.spins.forEach(function(spin) {
                    spin.life++;
                })
            }
        }
    } // end of Snake

    var snakeSize = prompt("Please Enter Snake Size", 3);
    var snakeObj = new Snake(snakeSize);

    function checkGameOver(positions) {
        for (var i = 0; i < positions.length; i++) {
            for (var j = 0; j < positions.length; j++) {
                if ((i!=j && positions[i].posX == positions[j].posX &&
                    positions[i].posY == positions[j].posY) ||
                    positions[i].posX == -1 || positions[i].posX == cols ||
                    positions[i].posY == -1 || positions[i].posY == rows) {
                    alert ("GameOver!");
                    snakeObj = new Snake(snakeSize);
                    break;
                }
            }
        }
    }

    /* function draws the matrix */
    function drawMatrix(rows, cols, snakeObj) {
        var stage = $('#stage').html('');
        for (var r = 0; r < rows; r += 1) {
            var row = $('<div class="row"></div>').appendTo(stage);
            for (var c = 0; c < cols; c += 1) {
                var col = $('<div class="col"></div>').appendTo(row);
            }
        }
    }


    function paintSnake(positions) {
        positions.forEach(function(pos) {
            var posX = (pos.posX + 1);
            var posY = (pos.posY + 1);
            $("#stage > .row:nth-child(" + posY + ") > .col:nth-child(" + posX +")").addClass('black');
        })
    }
    
    function deleteSnake(positions) {
        positions.forEach(function(pos) {
            var x = pos.posX + 1;
            var y = pos.posY + 1;
            console.warn("Delete " +x + "," + y);
            $("#stage > .row:nth-child(" + y + ") > .col:nth-child(" + x + ")").removeClass('black');
        });
    }


    $(document).ready(function () {
        drawMatrix(rows, cols, snakeObj);
        $('#instructions').html('<h3>Use the arrows to change the direction of the snake</h3>' +
           '<h3> Use the spacebar to feed the snake</h3>');
        setInterval(function() {
            deleteSnake(snakeObj.positions);
            snakeObj.movePixel();
            paintSnake(snakeObj.positions);
            console.log(snakeObj.positions);
        }, timeIncrement);
    });

    $(window).on('keydown', function (e) {
        if (e.keyCode == 37 && snakeObj.getSnakeDirection() != 'right') { // left
            snakeObj.createSpin('left');
        }
        if (e.keyCode == 38 && snakeObj.getSnakeDirection() != 'down') { // up
            snakeObj.createSpin('up');
        }
        if (e.keyCode == 39 && snakeObj.getSnakeDirection() != 'left') { // right
            snakeObj.createSpin('right');
        }
        if (e.keyCode == 40 && snakeObj.getSnakeDirection() != 'up') { // down
            snakeObj.createSpin('down');
        }
        if (e.keyCode == 32) {
            snakeObj.getFood();
        }
    });
})();